let fs = require("fs");
let csv = require("fast-csv");
function getTotalMatchesPerYear(matchData) {
    return new Promise(function (resolve, reject) {
        let totalMatchPerYear = [];
        fs.createReadStream(matchData).pipe(csv()).on("data", function (data) {
            if (Number(data[1])) {
                totalMatchPerYear.push(data[1]);
            }
        }).on("end", function () {
            var matches = totalMatchPerYear.reduce(noOfMatchesPerYear, {});
            function noOfMatchesPerYear(counter, year) {
                counter[year] = ++counter[year] || 1;
                return counter;
            }
            resolve(matches);
        });
    });
}

function getTotalMatchPerYear(matchFile) {
    return new Promise(function (resolve, reject) {
        let totalMatchPerYear = [];
        fs.createReadStream(matchFile).pipe(csv()).on("data", function (data) {
            if (Number(data[1])) {
                totalMatchPerYear.push(data[1]);
            }
        }).on("end", function () {
            totalMatchPerYear.sort(function (index1, index2) {
                return index1 -index2;
            })
            let seasonArray = [];
            let counter = 1;
            for (let i = 0; i < totalMatchPerYear.length; i++) {
                if (totalMatchPerYear[i] === totalMatchPerYear[i + 1]) {
                    counter++;
                } else {
                    seasonArray.push([totalMatchPerYear[i], counter]);
                    counter = 1;
                }
            }
            resolve(seasonArray);
        })
    });
}

function extraRunsConceded(matchFile, deliveriesFile) {
    return new Promise(function (resolve, reject) {
        let extraRuns = [];
        let matchID = [];
        let counter = 0;
        let teams = new Set();
        fs.createReadStream(deliveriesFile).pipe(csv()).on("data", function (data) {
            if (counter === 0) {
                fs.createReadStream(matchFile).pipe(csv()).on("data", function (data) {
                    if (data[1] == 2016) {
                        matchID.push(data[0]);
                    }
                })
                counter++;
            }
            if (matchID.includes(data[0])) {
                let teamObj = {};
                teams.add(data[2]);
                teamObj.match_id = data[0];
                teamObj.team = data[3];
                teamObj.extraRun = data[16];
                extraRuns.push(teamObj);
            }
        }).on("end", function () {
            let extraRunPerTeam = [];
            let extras = 0;
            for (let team of teams) {
                for (let index = 0; index < extraRuns.length - 1; index++) {
                    if (team === extraRuns[index].team) {
                        extras = extras + Number(extraRuns[index].extraRun);
                    }
                }
                let teamObj = {};
                teamObj.y = extras;
                teamObj.label = team;
                extraRunPerTeam.push(teamObj);
                extras = 0;
            }
            resolve(extraRunPerTeam);
        })
    });
}
// extraRunsConceded(matchFile,deliveriesFile);

function economicalBowler(matchFile, deliveriesFile) {
    return new Promise(function (resolve, reject) {
        let isChecked = true;
        let bowlers = new Set();
        let matchID = [];
        let economyRate = [];
        fs.createReadStream(deliveriesFile).pipe(csv()).on("data", function (data) {
            if (isChecked === true) {
                fs.createReadStream(matchFile).pipe(csv()).on("data", function (data) {
                    if (data[1] === "2015") {
                        matchID.push(data[0]);
                    }
                })
                isChecked = false;
            }
            if (matchID.includes(data[0])) {
                let teamObj = {};
                bowlers.add(data[8]);
                teamObj.bowler = data[8];
                teamObj.runs = data[17];
                teamObj.wideBall = data[10];
                teamObj.noBall = data[13];
                economyRate.push(teamObj);
            }
        }).on("end", function () {
            let totalRuns = 0;
            let totalBalls = 0;
            let bowlersEconomy = [];
            for (bowler of bowlers) {
                for (let i = 0; i < economyRate.length; i++) {
                    if (bowler === economyRate[i].bowler) {
                        if (economyRate[i].wideBall == 0 && economyRate[i].noBall == 0) {
                            totalBalls++;
                        }
                        totalRuns += Number(economyRate[i].runs);
                    }
                }
                let teamObj = {};
                let economy = ((totalRuns / totalBalls) * 6).toFixed(2);
                teamObj.bowler = bowler;
                teamObj.economy = economy;
                bowlersEconomy.push(teamObj);
                totalBalls = 0;
                totalRuns = 0;
            }
            bowlersEconomy.sort(function (index1, index2) {
                return index1.economy - index2.economy;
            })
            resolve(bowlersEconomy);
        });
    })
}

// economicalBowler(matchFile,deliveriesFile);

function getTopTenBowlers() {
    return new Promise(function (resolve, reject) {
        let tenBowler = [];
        let topTenBowler = economicalBowler(matchFile, deliveriesFile);
        topTenBowler.then(function (data) {
            for (let index = 0; index < 10; index++) {
                let teamObj = {};
                teamObj.y = Number(data[index].economy);
                teamObj.label = data[index].bowler;
                tenBowler.push(teamObj);
            }
            resolve(tenBowler);
        })
    })
}
// getTopTenBowlers();

function playerOfTheMatch(matchData) {
    return new Promise(function (resolve, reject) {
        let players = new Set();
        let playersArray = [];
        let manOfTheMatch = [];
        fs.createReadStream(matchData).pipe(csv()).on("data", function (data) {
            if(data[13]!=="player_of_match"){
                players.add(data[13]);
            }
                playersArray.push(data[13]);
        }).on("end", function () {
            for (player of players) {
                let count = 0;
                for (let i = 0; i < playersArray.length; i++) {
                    if (player === playersArray[i]) {
                        count++;
                    }
                }
                let teamObj = {};
                teamObj.player = player;
                teamObj.count = count;
                manOfTheMatch.push(teamObj);
            }
            manOfTheMatch.sort(function (index1, index2) {
                return index2.count - index1.count;
            })
            resolve(manOfTheMatch);
        })
    })
}

function getTopTenManOfTheMatch() {
    return new Promise(function (resolve, reject) {
        let tenPlayer = [];
        let topTenPlayer = playerOfTheMatch(matchFile);
        topTenPlayer.then(function (data) {
            for (let index = 0; index < 10; index++) {
                let teamObj = {};
                teamObj.y = Number(data[index].count);
                teamObj.label = data[index].player;
                tenPlayer.push(teamObj);
            }
            resolve(tenPlayer);
        })
    })
}
// getTopTenManOfTheMatch();

function getMatchId2016() {
    return new Promise(function (resolve, reject) {
        let matchID = [];
        require("fs").createReadStream(matchesTest2016).pipe(csv()).on("data", function (data) {
            if (data[1] == 2016) {
                matchID.push(data[0]);
            }
        }).on("end", function () {
            resolve(matchID);
        })
    })
}

function createJsonFile() {
    let totalMatch = getTotalMatchPerYear(matchFile);
    totalMatch.then(function (data) {
        require("fs").writeFile("matchesPerYear.json", JSON.stringify(data), (err) => {
            if (err) {
                throw err
            }
            console.log("The file has been saved!")
        })
    })

    let extraRun = extraRunsConceded(matchFile, deliveriesFile);
    extraRun.then(function (data) {
        require("fs").writeFile("extraRuns.json", JSON.stringify(data), (err) => {
            if (err) {
                throw err
            }
            console.log("The file has been saved!")
        })
    })
    let topTenBowlers = getTopTenBowlers();
    topTenBowlers.then(function (data) {
        require("fs").writeFile("topTenBowlers.json", JSON.stringify(data), (err) => {
            if (err) {
                throw err
            }
            console.log("The file has been saved!")
        })
    })
    let topTenPlayer = getTopTenManOfTheMatch();
    topTenPlayer.then(function (data) {
       
;        data.sort(function(a,b){
            return a.y-b.y;
        })
        
        require("fs").writeFile("jsonFiles/topTenPlayer.json", JSON.stringify(data), (err) => {
            if (err) {
                throw err
            }
            console.log("The file has been saved!")
        })
    })
}
// createJsonFile();

function matchWinnerPerYear(matchFile) {
    return new Promise(function (resolve, reject) {
        let winner = [];
        let teams = new Set();
        let winnersPerYear = [];
        fs.createReadStream(matchFile).pipe(csv()).on("data", function (data) {
            if (data[10] !== "" && data[10] !== "winner") {
                teams.add(data[10]);
            }
            let teamObj = {};
            teamObj.season = data[1];
            teamObj.winner = data[10];
            winnersPerYear.push(teamObj);
        }).on("end", function () {
            for (let team of teams) {
                let count = 0;
                for (let index = 1; index < winnersPerYear.length - 1; index++) {
                    if (index === winnersPerYear.length - 2) {
                        count++;
                    } else if (winnersPerYear[index].season === winnersPerYear[index + 1].season) {
                        if (team === winnersPerYear[index].winner) {
                            count++;
                        }
                    } else {

                        let teamObj = {};
                        teamObj.team = team;
                        teamObj.year = winnersPerYear[index].season;
                        teamObj.win = count;
                        winner.push(teamObj);
                        count = 0;

                    }
                }
                let teamObj = {};
                teamObj.team = team;
                teamObj.year = winnersPerYear[winnersPerYear.length - 1].season;
                teamObj.win = count;
                winner.push(teamObj);
            }
            resolve(winner);
        });
    })
}

// matchWinnerPerYear(matchFile);

function createMatchWinnerJsonFile() {
    let teamObj = matchWinnerPerYear(matchFile);
    teamObj.then(function (data) {
        let teams = new Set();
        let seasons = new Set();
        data.forEach(element => {
            teams.add(element.team);
            seasons.add(element.year);
        });
        let seasonArray = [];
        for (let season of seasons) {
            seasonArray.push(season);
        }
        let teamArray = [];
        let winArray = [];
        let result = [];
        for (let team of teams) {
            teamArray.push(team);
            for (let season of seasons) {
                let teamObj = {};
                teamObj["name"] = team;
                for (let i = 0; i < data.length; i++) {
                    if (team === data[i].team) {
                        winArray.push(data[i].win);
                    }
                }
                teamObj["data"] = winArray;
                winArray = [];
                result.push(teamObj);
                break;
            }
        }
        require("fs").writeFile("season.json", JSON.stringify(seasonArray), (err) => {
            if (err) {
                throw err
            }
            console.log("The file has been saved!")
        })
        require("fs").writeFile("stackBarChart.json", JSON.stringify(result), (err) => {
            if (err) {
                throw err
            }
            console.log("The file has been saved!")
        })
    })
}

// createMatchWinnerJsonFile();
module.exports = {
    getTotalMatchesPerYear: getTotalMatchesPerYear,
    getTotalMatchPerYear: getTotalMatchPerYear,
    matchWinnerPerYear: matchWinnerPerYear,
    extraRunsConceded: extraRunsConceded,
    economicalBowler: economicalBowler,
    playerOfTheMatch: playerOfTheMatch
}