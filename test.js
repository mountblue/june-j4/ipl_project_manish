let expect = require("chai").expect;
const path = require("path");
const matchFile = path.resolve("csv/matches.csv");
const deliveriesFile = path.resolve("csv/deliveries.csv");
const fileName = path.resolve("./ipldata");
const operations = require(fileName);
const dataset = path.resolve("csv/matchesTest.csv");
const dataset1 = path.resolve("csv/matchesTest1.csv");
const matchesTest2016 = path.resolve("csv/matchesTest2016.csv");
const deliveriTest2016 = path.resolve("csv/deliveriesTest2016.csv");
const matchesTest2015 = path.resolve("csv/matchesTest2015.csv");
const deliveriesTest2015 = path.resolve("csv/deliveriesTest2015.csv");

describe("unit testing Total match per year", function () {
	it("should return undefined", function (done) {
		const expectedResult = undefined;

		operations.getTotalMatchesPerYear(dataset).then(function (data) {
			try {
				expect(data).to.deep.not.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})

	it("should return null", function (done) {
		const expectedResult = null;

		operations.getTotalMatchesPerYear(dataset).then(function (data) {
			try {
				expect(data).to.deep.not.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})

	it("should return total number of matches per year in the form of object", function (done) {
		const expectedResult = {
			2015: 6,
			2016: 3,
			2017: 4,
			2018: 7
		}

		operations.getTotalMatchesPerYear(dataset).then(function (data) {
			try {
				expect(data).to.deep.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})

	it("should return total number of matches per year in the form of array of array", function (done) {
		const expectedResult = [
			["2015", 6],
			["2016", 3],
			["2017", 4],
			["2018", 7]
		];

		operations.getTotalMatchPerYear(dataset).then(function (data) {
			try {
				expect(data).to.deep.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})
})


describe("unit testing Mathces won by Teams", function () {
	it("Should return no such file or directory", function (done) {
		const expectedResult = " no such file or directory, open 'mathces.csv";

		operations.matchWinnerPerYear(dataset1).then(function (data) {
			try {
				expect(data).to.deep.not.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})
	it("should return number of matches won by per team year wise", function (done) {
		const expectedResult = [{
			"team": "Sunrisers Hyderabad",
			"win": 2,
			"year": "2017"
		},
		{
			"team": "Rising Pune Supergiant",
			"win": 1,
			"year": "2017"
		},
		{
			"team": "Kolkata Knight Riders",
			"win": 1,
			"year": "2017"
		}
		]

		operations.matchWinnerPerYear(dataset1).then(function (data) {
			try {
				expect(data).to.deep.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})
});

describe("unit testing Extra runs conceded by team", function () {

	it("should return extra runs conceded per team", function (done) {
		
		const expectedResult = [{
			label: "Kings XI Punjab",
			y: 6
		},
		{
			label: "Gujarat Lions",
			y: 1
		},
		{
			label: "Royal Challengers Bangalore",
			y: 11
		},
		{
			label: "Sunrisers Hyderabad",
			y: 7
		},
		{
			label: "Kolkata Knight Riders",
			y: 10
		},
		{
			label: "Mumbai Indians",
			y: 10
		},
		{
			label: "Rising Pune Supergiants",
			y: 0
		}
		]

		operations.extraRunsConceded(matchesTest2016, deliveriTest2016).then(function (data) {
			try {
				expect(data).to.deep.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})
});

describe("unit testing Extra runs conceded by team", function () {
	it("should return top economical bowler", function (done) {
		const expectedResult = [

			{
				"bowler": "A Mishra",
				"economy": "7.00"
			},
			{
				"bowler": "NM Coulter-Nile",
				"economy": "8.00"
			},
			{
				"bowler": "JP Duminy",
				"economy": "8.57"
			},
			{
				"bowler": "AD Mathews",
				"economy": "8.67"
			},
			{
				"bowler": "Imran Tahir",
				"economy": "8.75"
			},
			{
				"bowler": "Yuvraj Singh",
				"economy": "10.00"
			}
		]

		operations.economicalBowler(matchesTest2015, deliveriesTest2015).then(function (data) {
			try {
				expect(data).to.deep.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})

});

describe("Top ten man of the match mlayers in all seasons", function () {

	it("should return undefined", function (done) {
		const expectedResult = undefined;

		operations.playerOfTheMatch(dataset).then(function (data) {
			try {
				expect(data).to.deep.not.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})

	it("should return player name and total number of man of the match", function (done) {
		const expectedResult = [ 
		{ player: 'Yuvraj Singh', count: 1 },
		{ player: 'SPD Smith', count: 1 },
		{ player: 'CA Lynn', count: 1 }
	 ];

		operations.playerOfTheMatch(dataset1).then(function (data) {
			try {
				expect(data).to.deep.equal(expectedResult)
				done();
			} catch (e) {
				done(e);
			}
		})
	})
});