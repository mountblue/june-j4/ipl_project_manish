
function drawHighChart3() {
    let inputData = loadChart("jsonFiles/extraRuns.json");
    console.log(inputData);
    var chart = new CanvasJS.Chart("chartContainer", {
      animationEnabled: true,
      theme: "light2", // "light1", "light2", "dark1", "dark2"
      title: {
        text: "Extra runs conceded"
      },
      axisY: {
        title: "Extra Runs"
      },
      data: [{
        type: "column",
        showInLegend: true,
        legendMarkerColor: "grey",
        legendText: "Teams",
        dataPoints: inputData
      }]
    });
    chart.render();
  
  }
  
drawHighChart3();  