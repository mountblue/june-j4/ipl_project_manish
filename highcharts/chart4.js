
function drawHighChart4() {
    let inputData = loadChart("jsonFiles/topTenBowlers.json");
    console.log(inputData);
    var chart = new CanvasJS.Chart("chartContainerA", {
      animationEnabled: true,
      theme: "light2", // "light1", "light2", "dark1", "dark2"
      title: {
        text: "Top ten bowlers"
      },
      axisY: {
        title: "Economy"
      },
      data: [{
        type: "column",
        showInLegend: true,
        legendMarkerColor: "grey",
        legendText: "Bowlers",
        dataPoints: inputData
      }]
    });
    chart.render();
  }
  
drawHighChart4();  