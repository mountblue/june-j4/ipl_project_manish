
function drawHighChart1() {
    let inputData =loadChart("jsonFiles/matchesPerYear.json");
    Highcharts.chart("container", {
      chart: {
        type: "column"
      },
      title: {
        text: "Match played per year"
      },
  
      xAxis: {
        type: "category",
        labels: {
          rotation: -45,
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif"
          }
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: "win mathces"
        }
      },
      legend: {
        enabled: false
      },
      tooltip: {
        pointFormat: "Match won by teams"
      },
      series: [{
        name: "Matches",
        data: inputData,
        dataLabels: {
          enabled: true,
          rotation: -90,
          color: "#FFFFFF",
          align: "right",
          format: "{point.y:1f}",
          y: 10, 
          style: {
            fontSize: "13px",
            fontFamily: "Verdana, sans-serif"
          }
        }
      }]
    });
  }
  
  drawHighChart1();  