
function drawHighChart2() {
    let inputData = loadChart("jsonFiles/stackBarChart.json");
    let season = loadChart("jsonFiles/season.json");
    Highcharts.chart('barChart', {
      chart: {
        type: 'bar'
      },
      title: {
        text: 'Stacked bar chart'
      },
      xAxis: {
        categories: season
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Matches Won'
        }
      },
      legend: {
        reversed: true
      },
      plotOptions: {
        series: {
          stacking: 'normal'
        }
      },
      series: inputData
    });
  }

  drawHighChart2();